// Теоретичні питання

//     1. Опишіть своїми словами, що таке метод об'єкту.

//        Функція, яка здійснює дії з властивостями, методами обєкту. Крім того, виконує буль-які дії що й і звичайна функція.

//     2. Який тип даних може мати значення властивості об'єкта?

//      Undefined, Boolean, Number, String, BigInt, Symbol, Null Object.

//     3. Об'єкт це посилальний тип даних. Що означає це поняття?

//         Об'єкт передається по ссилці. Наприклад const newOdj = {}; const newObj2 = newObj; де у змінних зберігається ссилка на один і той же самий об'єкт.

// Завдання

let number1Hint = document.querySelector('.number1-hint');
let number2Hint = document.querySelector('.number2-hint');

const errorHandler = (text, place) => {
    if (place === 'firstName') {
        number1Hint.innerText = text;
    } else if (place === 'secondName') {
        number2Hint.innerText = text;
    }
};

const action1 = document.querySelector('.input-num1');
const action2 = document.querySelector('.input-num2');

action1.addEventListener('input', e => {
    e.target.value = e.target.value.replace(/\d/g, '');
});
action2.addEventListener('input', e => {
    e.target.value = e.target.value.replace(/\d/g, '');
});

const validateNumber = (firstName, secondName) => {
    let switcher = true;

    if (!firstName) {
        errorHandler("Ви не ввели ім'я!", 'firstName');

        switcher = false;
    }

    if (!secondName) {
        errorHandler('Ви не ввели фамілію!', 'secondName');

        switcher = false;
    }

    if (!!firstName && Number(firstName)) {
        errorHandler("Ви ввели некоректне ім'я!", 'firstName');

        switcher = false;
    }

    if (!!secondName && Number(secondName)) {
        errorHandler('Ви ввели некоректну фамілію!', 'secondName');

        switcher = false;
    }

    return switcher;
};

const getAction = () => {
    const firstName = document.querySelector('.input-num1');
    const secondName = document.querySelector('.input-num2');
    const output = document.querySelector('.output');

    number1Hint.innerText = '';
    number2Hint.innerText = '';
    output.innerText = '';

    if (validateNumber(firstName.value, secondName.value)) {
        const user = createNewUser(firstName.value, secondName.value);

        output.innerText = 'Login: ' + user.getLogin();
        console.log('Login: ' + user.getLogin());
    }
};

const createNewUser = (firstName = 'Empty', secondName = 'Empty') => {
    const newUser = {
        _firstName: firstName,
        _secondName: secondName,

        get firstName() {
            return this._firstName;
        },

        get secondName() {
            return this._secondName;
        },

        set firstName(name) {
            this._firstName = name;
        },

        set secondName(name) {
            this._secondName = name;
        },

        getLogin() {
            return (this._firstName.trim()[0] + this._secondName).toLowerCase();
        },
    };

    return newUser;
};

const onSubmit = () => {
    const submitBtn = document.querySelector('.submit');

    submitBtn.addEventListener('click', getAction);
};

onSubmit();
